<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//
Route::group(['prefix' => 'admin'], function () {


    Voyager::routes();
//    Route::get('/users/wallet_configuration', 'Admin\UserController@walletConfiguration')->name('users.wallet_configuration');
    Route::get('users_wallet_configuration/{id}', ['uses' => 'App\Http\Controllers\Admin\UserController@walletConfiguration', 'as' => 'users.wallet_configuration']);





});
Route::get('/{vue_capture?}', ['uses' => 'App\Http\Controllers\frontend\HomeController@welcome'])->where('vue_capture', '[\/\w\.-]*');

