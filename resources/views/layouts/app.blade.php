<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .vld-overlay.is-active {
            position: absolute;
            left: 50%;
            top: 50%;
            right: 50%;
            bottom: 50%;
            width: 100%;
            height: 100%;
            z-index: 9999;

        }


    </style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script defer src="{{ asset('js/app.js') }}"></script>
    <script>
        var laravel = @json(['baseURL' => url('/')])
    </script>
    <script>
        window._asset = '{{ asset('') }}';
    </script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div id="app" class="w-100 milage">

    <div class="row">
        <top-header class="col-12"></top-header>
    </div>
    <div class="row bg-danger">
        <header-bar-categories></header-bar-categories>
    </div>

    <div class="" style="background-color: #F2F2F2;">
        <div class="container p-0">
            <div class="row">
                <div class="col-12">
                    <router-view></router-view>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <main-footer></main-footer>
        </div>
    </div>
</div>
</body>
</html>
