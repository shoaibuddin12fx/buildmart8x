var json = require('../../js/config/config.json');



export default class ApiService {

    constructor() {
    }

    get(api) {
        console.log({api})
        return new Promise( resolve => {
            window.axios.get(json.base_url+api).then( res => {
                resolve(res.data);
            });
        })

    }

    post(api, data, config = {}) {
        return new Promise( resolve => {
            window.axios.post(json.base_url + api, data, config).then(res => {
                resolve(res.data);
            });
        })
    }

    put(api, data, config = {}) {
        return Vue.axios.put(json.base_url+api, data, config);
    }
    delete(api, id, config = {}) {
        return Vue.axios.delete(json.base_url+api, id);
    }

}
