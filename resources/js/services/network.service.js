import ApiService from "./api.service";
const apiService = new ApiService();
import UtilityService from "./utility.service";
const utilityService = new UtilityService();


export default class NetworkService {

    constructor() {
    }

    getMenus(){
        return this.axiosGetResponse('menus', null, false)
    }

    getSiteLogo(){
        return this.axiosGetResponse('site_logo', null, false)

    }

    testGet(){
       return this.axiosGetResponse('https://jsonplaceholder.typicode.com/todos', 1, true)
    }

    testPost(data){
        return this.axiosPostResponse('https://reqres.in/api/users', data, null, true)
    }

    axiosGetResponse(key, id = null, showLoader = false, showError = true, contentType = 'application/json') {
        return this.httpResponse('get', key, {}, id, showLoader, showError, contentType);
    }

    axiosPostResponse(key, data, id = null, showLoader = false, showError = true, contentType = 'application/json') {
        return this.httpResponse('post', key, data, id, showLoader, showError, contentType);
    }

    axiosPutResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return this.httpResponse('put', key, data, id, showloader, showError, contenttype);
    }



    // axiosPostResponse(api, data, config = {}) {
    //     return apiService.post(api, data, config);
    // }





    httpResponse(type = 'get', key, data, id = null, showLoader = false, showError = true, contentType = 'application/json') {

        return new Promise( ( resolve, reject ) => {

            if (showLoader == true) {
               utilityService.showLoader();
            }

            const _id = (id) ? '/' + id : '';
            const url = key + _id;
            console.log({url, _id});
            const seq = (type == 'get') ? apiService.get(url, {}) : ((type == 'put') ? apiService.put(url, data) : apiService.post(url, data) ) ;
            console.log({seq});

            seq.then((res) => {
                console.log({res});

                if(res.status != 200){
                    if(showError == true){
                        // this.utility.presentFailureToast(res['message']);
                    }

                    reject(null);
                    return;
                }
                resolve(res.data);


                // this.utility.presentSuccessToast(res['message']);

            }, err => {

                let error = err['error'];
                if (showLoader == true) {
                    // this.utility.hideLoader();
                }

                if (showError == true) {
                    // this.utility.presentFailureToast(error['message']);
                }

                console.log({err});

                reject(err);

            });

        });

    }
}
