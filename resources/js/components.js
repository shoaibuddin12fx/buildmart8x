
// Declare All the Components Here

Vue.component('top-header', require('./components/header/TopHeaderComponent').default);
Vue.component('main-menu', require('./components/header/MenuComponent/MenuComponent').default);
Vue.component('nav-search', require('./components/header/HeaderSearch/HeaderSearchComponent').default);
Vue.component('header-meta', require('./components/header/HeaderMeta/HeaderMetaComponent').default);
Vue.component('header-bar-categories', require('./components/header/HeaderBarCategories/HeaderBarCategories').default);
Vue.component('header-slider', require('./components/header/HeaderSlider/HeaderSlider').default);
Vue.component('main-footer', require('./components/footer/FooterComponent').default);

Vue.component('sign-in-page', require('./pages/signin-page/SignInPageComponent').default);

// modals
Vue.component('login-modal', require('./components/user/LoginModal').default);
